<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [

                'name' => '5 Days',
                'percent' => '1.40',
                'color' => '5',
                'icon' => 'fa-laptop',
                'children' => [

                    ['name' => '40% in 5 Days'],


                     ]
            ],
            [
                'name' => '10 Days',
                'percent' => '1.80',
                'color' => '10',
                'icon' => 'fa-university',
                'children' => [
                    ['name' => '80% in 10 Days'],

                    ]
            ],
            



        ];

        foreach ($categories as $category) {
            \openjobs\Category::create($category);
        }
    }
}

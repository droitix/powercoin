<?php

use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(openjobs\Plan::class, function (Faker $faker) {
    return [
        'braintree_id' => str_random(10),
        'price' => random_int(1, 1000),
        'active' => false,
    ];
});

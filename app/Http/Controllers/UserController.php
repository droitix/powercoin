<?php

namespace openjobs\Http\Controllers;

use Illuminate\Http\Request;
use openjobs\Jobs\UserViewedResume;
use openjobs\User;
use openjobs\Resume;
use openjobs\Area;
use openjobs\File;
use Auth;
use Image;

class UserController extends Controller
{


public function getProfile(Request $request, Area $area, Resume $resume,File $file, $user_id)
{
          $user = User::where('id', $user_id)->first();
          if (!$user) {
            abort(404);
          }

  $resumes = $user->resumes()->isLive()->latestFirst()->paginate(10);
  $files = $user->files()->paginate(10);


          return view('profile.index', compact('resumes','files','file','resume','user', $user));
}


    public function profile()
  {
    return view('profile.profile', array('user' => Auth::user()));
  }


   public function update_avatar(Request $request)
  {
      if($request->hasFile('avatar')){
        $avatar = $request->file('avatar');
        $filename = time() . '.' . $avatar->getClientOriginalExtension();
        Image::make($avatar)->resize(300, 300)->save( public_path('/uploads/avatars/' .$filename) );

        $user = Auth::user();
        $user->avatar = $filename;
        $user->save();
      }
      notify()->success('Profile picture changed!');
      return redirect()->back()->withInput();
  }
    public function update(Request $request)
  {
    $request = $request->validate([
      'email' => 'required',
      'name' => '',
      'surname' => '',
      'phone' => '',
      'bank' => '',
      'btcaddress' => '',
      'account' => '',
      'accounttype' => '',
      'branch' => '',




    ]);

    $saved = false;
    $user = auth()->user();
    $user->name = $request['name'];
    $user->surname = $request['surname'];
    $user->phone = $request['phone'];
    $user->email = $request['email'];
    $user->bank = $request['bank'];
    $user->account = $request['account'];
    $user->btcaddress = $request['btcaddress'];
    $user->accounttype = $request['accounttype'];
    $user->branch = $request['branch'];



    if($user->save()) $saved = true;

    notify()->success('Information edited succesifully!');

    return view('profile.profile', compact('saved', 'user'));
  }

}

<?php

namespace openjobs\Http\Controllers\Listing;

use openjobs\{Area, Category, Listing};
use Illuminate\Http\Request;
use openjobs\Http\Controllers\Controller;
use openjobs\Http\Resources\CommentResource;

class ListingCommentController extends Controller
{
     public function index(Request $request, Area $area, Listing $listing)
    {
       return CommentResource::collection($listing->comments()->with(['children','user'])->get());
    }

    public function store(Request $request, Area $area, Listing $listing)

    {


        $this->validate($request, [
            'body' => 'required|max:5000'
        ]);

        $comment = $listing->comments()->make([
            'body' => $listing->amount*($listing->category->parent->percent);
        ]);
        $comment->body = $listing->amount*($listing->category->parent->percent);
        $request->user()->comments()->save($comment);

         return new CommentResource($comment);

    }
}

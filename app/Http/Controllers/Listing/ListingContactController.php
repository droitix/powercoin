<?php

namespace openjobs\Http\Controllers\Listing;

use Mail;
use Illuminate\Http\Request;
use openjobs\Http\Controllers\Controller;
use openjobs\{Area, Listing};
use openjobs\Http\Requests\StoreListingContactFormRequest;
use openjobs\Mail\ListingContactCreated;

class ListingContactController extends Controller
{
    public function __construct()
    {
        return $this->middleware(['auth']);
    }

    public function store(StoreListingContactFormRequest $request, Area $area, Listing $listing)
    {
        Mail::to($listing->user)
        ->cc($listing->companyemail)
        ->queue(
            new ListingContactCreated($listing, $request->user(), $request->message)
        );

        return back()->withSuccess("We have sent your message");
    }
}

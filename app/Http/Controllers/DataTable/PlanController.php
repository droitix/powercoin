<?php

namespace openjobs\Http\Controllers\DataTable;

use openjobs\Plan;
use Illuminate\Http\Request;
use openjobs\Http\Controllers\DataTable\DataTableController;

class PlanController extends DataTableController
{
  

    public function builder()
    {
        return Plan::query();
    }


    
}

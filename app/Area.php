<?php

namespace openjobs;

use Kalnoy\Nestedset\NodeTrait;
use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    use NodeTrait;

    protected $fillable = ['name', 'slug'];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function listings()
    {
        return $this->hasMany(Listing::class);
    }

    public function scopeWithListings($query)
    {
        return $query->with(['listings' => function ($q) {
            $q->isLive();
        }]);
    }

}

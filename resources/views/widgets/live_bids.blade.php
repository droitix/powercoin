 @if ($time >= $morningstart && $time <= $morningend)

 <div class="main-content-wrap sidenav-open d-flex flex-column">
            <!-- ============ Body content start ============= -->
            <div class="main-content">
                <div class="breadcrumb">
                    <h1>Auction in progress</h1>
                    <ul>

                    </ul>
                </div>
                <div class="separator-breadcrumb border-top"></div>

                <!-- CARD ICON-->
                <div class="row">
@foreach ($listings as $listing)

 @php ($sum = 0)

                                  @foreach($listing->comments as $comment)

                                   @php ($sum += $comment->split)



                                  @endforeach

        @if ($listing->amount > $sum)

                            <div class="col-md-4">
                                <a href="{{ route('listings.apply', [$area, $listing]) }}">
                                <div class="card card-icon mb-4">
                                    <div class="card-body text-center"><i class="i-Money-2"></i>
                                        <h2>R {{$listing->amount - $sum}}</h2>
                                        <p class="lead text-22 m-0">{{$listing->user->bank}}</p>
                                    </div>
                                </div>
                                </a>
                            </div>


        @else



        @endif


@endforeach


                </div><!-- end of main-content -->
            </div><!-- Footer Start -->
@else



 <div class="main-content-wrap sidenav-open d-flex flex-column">
            <!-- ============ Body content start ============= -->
            <div class="main-content">
                <div class="breadcrumb">
                    <h1> NEXT AUCTION 7pm</h1>
                    <ul>

                    </ul>
                </div>
                <div class="separator-breadcrumb border-top"></div>


 </div><!-- Footer Start -->

@endif

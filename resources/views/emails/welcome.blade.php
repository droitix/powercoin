<!DOCTYPE html>
<html dir="ltr">
 <head>
  <meta charset="utf-8" />
  <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible" />
  <title>
  </title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport" />
  <meta content="telephone=no" name="format-detection" />
 </head>
 <body style="background-color: #fff; color: #1e1c2c; font-family: Arial, sans-serif; font-size: 15px; font-style: normal; line-height: 20px; margin: 0; padding: 20px 0;">
  <table bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" style="border: none; margin: 0 auto; max-width: 580px; min-width: 280px; width: 100%;" width="100%">
   <tbody>
    <tr>

    </tr>
    <tr>
     <td style="padding: 40px 0;">
      <table border="0" cellpadding="0" cellspacing="0" style="border: none;" width="100%">
       <tr>
        <td style="font-size: 20px; font-weight: bold; padding-bottom: 20px; padding-left: 10px; padding-right: 10px;">Welcome, {{$user->name}}</td>
       </tr>
       <tr>
        <td style="padding-bottom: 20px; padding-left: 10px; padding-right: 10px;">Thanks for creating your RevivalCoin account!</td>
       </tr>
       <tr>

       </tr>
      </table>
      <table border="0" cellpadding="0" cellspacing="0" style="border: none;" width="100%">


         </table>

         <table border="0" cellpadding="0" cellspacing="0" style="border: none;" width="100%">

         </table>
        </td>
       </tr>
       <tr>
        <td style="padding-bottom: 20px; padding-top: 20px;">
         <table border="0" cellpadding="0" cellspacing="0" style="border: none;" width="100%">
          <tr>

          </tr>
         </table>
        </td>
       </tr>
      </table>
     </td>
    </tr>
    <tr>
     <td style="border-top: 1px solid #e8eaf0; padding: 20px 0; padding-left: 10px; padding-right: 10px;"><span style="color: #acacad; display: inline-block; font-weight: bold; letter-spacing: -0.5px; margin-right: 10px;">&copy; 2020 RevivalCoin</span> <a href="https://www.revivalcoin.co.za/" style="color: #428ee6; display: inline-block; margin-right: 10px; text-decoration: none;">About Us</a></td>
    </tr>
   </tbody>
  </table>
 </body>
</html>

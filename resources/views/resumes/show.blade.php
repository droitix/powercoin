@extends('layouts.userapp')

@section('title')
   Edit My Resume  | Openjobs360
@endsection

@section('content')

   <!-- Our Dashbord -->
    <section class="our-dashbord dashbord">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-lg-4 col-xl-3 dn-smd">
                    <div class="user_profile">
                        <div class="media">
                             <img src="/uploads/avatars/{{ Auth::user()->avatar }}" class="align-self-start mr-3 rounded-circle" alt="e1.png">

                            <div class="media-body">
                                <h5 class="mt-0">Hi, {{ Auth::user()->fullname }}</h5>
                                <p>{{ Auth::user()->province }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="dashbord_nav_list">
                           <ul>

                            <li class="active"><a href="{{ route('profile') }}"><span class="flaticon-profile"></span> Profile</a></li>
                            <li><a href="{{ route('resumes.published.index', [$area]) }}"><span class="flaticon-resume"></span> Manage Resume</a></li>
                            <li><a href="#"><span class="flaticon-paper-plane"></span> Applied Jobs</a></li>
                            <li><a href="#"><span class="flaticon-analysis"></span> CV Manager</a></li>
                            <li><a href="{{ route('listings.favourites.index', [$area]) }}"><span class="flaticon-favorites"></span> Saved Jobs</a></li>
                            <li ><a href="{{ route('listings.viewed.index', [$area]) }}"><span class="flaticon-eye"></span> Jobs You Viewed</a></li>

                            <li><a  href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                            <span class="flaticon-logout"></span>{{ __('Logout') }}
                                        </a></li>

                                         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>


                        </ul>
                    </div>

                </div>
                <div class="col-sm-12 col-lg-8 col-xl-9">
                    <div class="row">
                        <div class="col-lg-12">
                            <h4 class="fz20 mb20">My Resume</h4>
                        </div>


                        <div class="col-lg-12">
                            <div class="my_resume_eduarea">
                                <h4 class="title">Education <small class="float-right"><a href="{{ route('resumes.eduedit', [$area, $resume]) }}">Add New Education <span class="flaticon-right-arrow"></span></a></small></h4>
                                <div class="content">
                                    <div class="circle"></div>
                                    <p class="edu_center">{{$resume->institution1}} <small>{{$resume->edufromyear1}}-{{$resume->edutoyear1}}</small></p>
                                    <p class="edu_center"><small>{{$resume->edulevel1}}</small></p>
                                    <h4 class="edu_stats">{{$resume->edutitle1}}
                                        <ul class="edu_stats_list float-right">
                                            <li class="list-inline-item"><a href="{{ route('resumes.eduedit', [$area, $resume]) }}" data-toggle="tooltip" data-placement="top" title="Edit"><span class="flaticon-edit"></span></a></li>
                                            <li class="list-inline-item"><a href="#" data-toggle="tooltip" data-placement="top" title="Delete"><span class="flaticon-rubbish-bin"></span></a></li>
                                        </ul>
                                    </h4>

                                </div>
                                <div class="content">
                                    <div class="circle"></div>
                                    <p class="edu_center">{{$resume->institution2}} <small>{{$resume->edufromyear2}}-{{$resume->edutoyear2}}</small></p>
                                    <p class="edu_center"><small>{{$resume->edulevel2}}</small></p>
                                    <h4 class="edu_stats">{{$resume->edutitle2}}
                                        <ul class="edu_stats_list float-right">
                                            <li class="list-inline-item"><a href="{{ route('resumes.eduedit', [$area, $resume]) }}" data-toggle="tooltip" data-placement="top" title="Edit"><span class="flaticon-edit"></span></a></li>
                                            <li class="list-inline-item"><a href="#" data-toggle="tooltip" data-placement="top" title="Delete"><span class="flaticon-rubbish-bin"></span></a></li>
                                        </ul>
                                    </h4>

                                </div>
                                <div class="content">
                                    <div class="circle"></div>
                                    <p class="edu_center">{{$resume->institution3}} <small>{{$resume->edufromyear3}}-{{$resume->edutoyear3}}</small></p>
                                    <p class="edu_center"><small>{{$resume->edulevel3}}</small></p>
                                    <h4 class="edu_stats">{{$resume->edutitle3}}
                                        <ul class="edu_stats_list float-right">
                                            <li class="list-inline-item"><a href="{{ route('resumes.eduedit', [$area, $resume]) }}" data-toggle="tooltip" data-placement="top" title="Edit"><span class="flaticon-edit"></span></a></li>
                                            <li class="list-inline-item"><a href="#" data-toggle="tooltip" data-placement="top" title="Delete"><span class="flaticon-rubbish-bin"></span></a></li>
                                        </ul>
                                    </h4>

                                </div>
                                 <div class="content">
                                    <div class="circle"></div>
                                    <p class="edu_center">{{$resume->institution4}} <small>{{$resume->edufromyear4}}-{{$resume->edutoyear4}}</small></p>
                                    <p class="edu_center"><small>{{$resume->edulevel4}}</small></p>
                                    <h4 class="edu_stats">{{$resume->edutitle4}}
                                        <ul class="edu_stats_list float-right">
                                            <li class="list-inline-item"><a href="{{ route('resumes.eduedit', [$area, $resume]) }}" data-toggle="tooltip" data-placement="top" title="Edit"><span class="flaticon-edit"></span></a></li>
                                            <li class="list-inline-item"><a href="#" data-toggle="tooltip" data-placement="top" title="Delete"><span class="flaticon-rubbish-bin"></span></a></li>
                                        </ul>
                                    </h4>

                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="my_resume_eduarea">
                                <h4 class="title">Work & Experience <small class="float-right"><a href="{{ route('resumes.compedit', [$area, $resume]) }}">Add New Work Experience <span class="flaticon-right-arrow"></span></a></small></h4>
                                <div class="content">
                                    <div class="circle"></div>
                                    <p class="edu_center">{{$resume->comp1}}<small>{{$resume->compfromyear1}}-{{$resume->comptoyear1}}</small></p>
                                    <h4 class="edu_stats">{{$resume->comp1title}}
                                        <ul class="edu_stats_list float-right">
                                            <li class="list-inline-item"><a href="{{ route('resumes.compedit', [$area, $resume]) }}" data-toggle="tooltip" data-placement="top" title="Edit"><span class="flaticon-edit"></span></a></li>
                                            <li class="list-inline-item"><a href="#" data-toggle="tooltip" data-placement="top" title="Delete"><span class="flaticon-rubbish-bin"></span></a></li>
                                        </ul>
                                    </h4>
                                    <p>{{$resume->comp1about}}</p>
                                </div>
                                <div class="content style2">
                                    <div class="circle"></div>
                                    <p class="edu_center">{{$resume->comp2}}<small>{{$resume->compfromyear2}}-{{$resume->comptoyear2}}</small></p>
                                    <h4 class="edu_stats">{{$resume->comp2title}}
                                        <ul class="edu_stats_list float-right">
                                            <li class="list-inline-item"><a href="{{ route('resumes.compedit', [$area, $resume]) }}" data-toggle="tooltip" data-placement="top" title="Edit"><span class="flaticon-edit"></span></a></li>
                                            <li class="list-inline-item"><a href="#" data-toggle="tooltip" data-placement="top" title="Delete"><span class="flaticon-rubbish-bin"></span></a></li>
                                        </ul>
                                    </h4>
                                    <p>{{$resume->comp2about}}</p>
                                </div>
                                 <div class="content style2">
                                    <div class="circle"></div>
                                    <p class="edu_center">{{$resume->comp3}}<small>{{$resume->compfromyear3}}-{{$resume->comptoyear3}}</small></p>
                                    <h4 class="edu_stats">{{$resume->comp3title}}
                                        <ul class="edu_stats_list float-right">
                                            <li class="list-inline-item"><a href="{{ route('resumes.compedit', [$area, $resume]) }}" data-toggle="tooltip" data-placement="top" title="Edit"><span class="flaticon-edit"></span></a></li>
                                            <li class="list-inline-item"><a href="#" data-toggle="tooltip" data-placement="top" title="Delete"><span class="flaticon-rubbish-bin"></span></a></li>
                                        </ul>
                                    </h4>
                                    <p>{{$resume->comp3about}}</p>
                                </div>

                                <div class="content">
                                    <div class="circle"></div>
                                    <p class="edu_center">{{$resume->comp4}}<small>{{$resume->compfromyear4}}-{{$resume->comptoyear4}}</small></p>
                                    <h4 class="edu_stats">{{$resume->comp4title}}
                                        <ul class="edu_stats_list float-right">
                                            <li class="list-inline-item"><a href="{{ route('resumes.compedit', [$area, $resume]) }}" data-toggle="tooltip" data-placement="top" title="Edit"><span class="flaticon-edit"></span></a></li>
                                            <li class="list-inline-item"><a href="#" data-toggle="tooltip" data-placement="top" title="Delete"><span class="flaticon-rubbish-bin"></span></a></li>
                                        </ul>
                                    </h4>
                                    <p>{{$resume->comp4about}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="col-lg-12">
                            <div class="my_resume_eduarea">
                                <h4 class="title">Certifications <small class="float-right"><a href="{{ route('resumes.edit', [$area, $resume]) }}">Add New Certifications <span class="flaticon-right-arrow"></span></a></small></h4>
                                <div class="content">
                                    <div class="circle"></div>
                                    <p class="edu_center">Jan 2018</p>
                                    <h4 class="edu_stats">Perfect Attendance Programs
                                        <ul class="edu_stats_list float-right">
                                            <li class="list-inline-item"><a href="#" data-toggle="tooltip" data-placement="top" title="Edit"><span class="flaticon-edit"></span></a></li>
                                            <li class="list-inline-item"><a href="#" data-toggle="tooltip" data-placement="top" title="Delete"><span class="flaticon-rubbish-bin"></span></a></li>
                                        </ul>
                                    </h4>

                                </div>
                                    <div class="content">
                                    <div class="circle"></div>
                                    <p class="edu_center">Jan 2018</p>
                                    <h4 class="edu_stats">Perfect Attendance Programs
                                        <ul class="edu_stats_list float-right">
                                            <li class="list-inline-item"><a href="#" data-toggle="tooltip" data-placement="top" title="Edit"><span class="flaticon-edit"></span></a></li>
                                            <li class="list-inline-item"><a href="#" data-toggle="tooltip" data-placement="top" title="Delete"><span class="flaticon-rubbish-bin"></span></a></li>
                                        </ul>
                                    </h4>

                                </div>
                                <div class="content style2">
                                    <div class="circle"></div>
                                    <p class="edu_center">Dec 2019</p>
                                    <h4 class="edu_stats">Top Performer Recognition
                                        <ul class="edu_stats_list float-right">
                                            <li class="list-inline-item"><a href="#" data-toggle="tooltip" data-placement="top" title="Edit"><span class="flaticon-edit"></span></a></li>
                                            <li class="list-inline-item"><a href="#" data-toggle="tooltip" data-placement="top" title="Delete"><span class="flaticon-rubbish-bin"></span></a></li>
                                        </ul>
                                    </h4>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

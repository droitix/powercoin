<div class="my_profile_select_box form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
            <label for="formGroupExampleInput1">Select  Sector</label><br>
                                    <select name="category_id" id="category" class="selectpicker"{{ isset($listing) && $listing->live() ? ' disabled="disabled"' : '' }}>
                                        @foreach ($categories as $category)
            <optgroup label="{{ $category->name }}">
                @foreach ($category->children as $child)
                    @if (isset($listing) && $listing->category_id == $child->id || old('category_id') == $child->id)
                        <option value="{{ $child->id }}" selected="selected">{{ $child->name }} </option>
                    @else
                        <option value="{{ $child->id }}">{{ $child->name }}</option>
                    @endif
                @endforeach
            </optgroup>
        @endforeach
                                    </select>
                                     @if ($errors->has('category_id'))
        <span class="help-block">
            {{ $errors->first('category_id') }}
        </span>
    @endif
 </div>


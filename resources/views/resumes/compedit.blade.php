@extends('layouts.userapp')
@section('title')
  Edit Your Work Experience  | Openjobs360
@endsection
@section('content')
<!-- Our Dashbord -->
    <section class="our-dashbord dashbord">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-lg-4 col-xl-3 dn-smd">
                    <div class="user_profile">
                        <div class="media">
                            <img src="/uploads/avatars/{{ Auth::user()->avatar }}" class="align-self-start mr-3 rounded-circle" alt="e1.png">

                            <div class="media-body">
                                <h5 class="mt-0">Hi, {{ Auth::user()->fullname }}</h5>
                                <p>{{ Auth::user()->province }}</p>
                            </div>

                        </div>
                    </div>
                    <div class="dashbord_nav_list">
                             <ul>

                            <li><a href="{{ route('profile') }}"><span class="flaticon-profile"></span> Profile</a></li>
                            <li class="active"><a href="{{ route('resumes.published.index', [$area]) }}"><span class="flaticon-resume"></span> Manage Resume</a></li>
                            <li><a href="#"><span class="flaticon-paper-plane"></span> Applied Jobs</a></li>
                            <li><a href="#"><span class="flaticon-analysis"></span> CV Manager</a></li>
                            <li><a href="{{ route('listings.favourites.index', [$area]) }}"><span class="flaticon-favorites"></span> Saved Jobs</a></li>
                            <li ><a href="{{ route('listings.viewed.index', [$area]) }}"><span class="flaticon-eye"></span> Jobs You Viewed</a></li>

                            <li><a  href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                            <span class="flaticon-logout"></span>{{ __('Logout') }}
                                        </a></li>

                                         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>


                        </ul>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-8 col-xl-9">
                    <div class="my_profile_form_area">
                   <form action="{{ route('resumes.update', [$area, $resume]) }}" method="post">
                        <div class="row">
                            <div class="col-lg-12">
                                <h4 class="fz20 mb20">Continue editing your resume</h4>
                            </div>

                            <div class="col-lg-12 mt30">
                                <div class="my_profile_thumb_edit"></div>
                            </div>



                         <div class="col-lg-12">
                                <h4 class="fz20 mb20">Add your work history</h4>
                            </div>

                           <div class="col-md-6 col-lg-6">
                                <div class="my_profile_input form-group">
                                    <label for="formGroupExampleInput1">Company Name</label>
                                    <input type="text" class="form-control" id="formGroupExampleInput1" name="comp1" placeholder="Droitix Robotics" value="{{$resume->comp1}}">
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <div class="my_profile_input form-group">
                                    <label for="formGroupExampleInput2">City/Town</label>
                                    <input type="text" class="form-control" id="formGroupExampleInput2" name="comp1loc" placeholder="Cape Town" value="{{$resume->comp1loc}}">
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <div class="my_profile_input form-group">
                                    <label for="exampleFormControlInput2">Job Title</label>
                                    <input type="text" class="form-control" id="exampleFormControlInput2" name="comp1title" placeholder="Lead Developer" value="{{$resume->comp1title}}">
                                </div>
                            </div>



                           <div class="col-md-6 col-lg-3">
                                <div class="my_profile_select_box form-group">

                        <label for="exampleFormControlInput3">Start Year</label><br>
                         <select name="compfromyear1" id="position" class="selectpicker">

                             @include('resumes.partials.forms.compstart')
                                 </select>
                                </div>
                            </div>
                             <div class="col-md-6 col-lg-3">
                                <div class="my_profile_select_box form-group">

                        <label for="exampleFormControlInput3">Finished</label><br>
                         <select name="comptoyear1" id="position" class="selectpicker">

                             @include('resumes.partials.forms.compfinish')
                                 </select>
                                </div>
                            </div>
                             <div class="col-lg-12">
                                <h4 class="fz20 mb20">Add your other work experience</h4>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <div class="my_profile_input form-group">
                                    <label for="formGroupExampleInput1">Company Name</label>
                                    <input type="text" class="form-control" id="formGroupExampleInput1" name="comp2"  value="{{$resume->comp2}}">
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <div class="my_profile_input form-group">
                                    <label for="formGroupExampleInput2">City/Town</label>
                                    <input type="text" class="form-control" id="formGroupExampleInput2" name="comp2loc"  value="{{$resume->comp2loc}}">
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <div class="my_profile_input form-group">
                                    <label for="exampleFormControlInput2">Job Title</label>
                                    <input type="text" class="form-control" id="exampleFormControlInput2" name="comp2title"  value="{{$resume->comp2title}}">
                                </div>
                            </div>



                           <div class="col-md-6 col-lg-3">
                                <div class="my_profile_select_box form-group">

                        <label for="exampleFormControlInput3">Start Year</label><br>
                         <select name="compfromyear2" id="position" class="selectpicker">

                             @include('resumes.partials.forms.compstart2')
                                 </select>
                                </div>
                            </div>
                             <div class="col-md-6 col-lg-3">
                                <div class="my_profile_select_box form-group">

                        <label for="exampleFormControlInput3">Finished</label><br>
                         <select name="comptoyear2" id="position" class="selectpicker">

                             @include('resumes.partials.forms.compfinish2')
                                 </select>
                                </div>
                            </div>


                                                         <div class="col-lg-12">
                                <h4 class="fz20 mb20">More experience will give you an edge</h4>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <div class="my_profile_input form-group">
                                    <label for="formGroupExampleInput1">Company Name</label>
                                    <input type="text" class="form-control" id="formGroupExampleInput1" name="comp3"  value="{{$resume->comp3}}">
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <div class="my_profile_input form-group">
                                    <label for="formGroupExampleInput2">City/Town</label>
                                    <input type="text" class="form-control" id="formGroupExampleInput2" name="comp3loc"  value="{{$resume->comp3loc}}">
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <div class="my_profile_input form-group">
                                    <label for="exampleFormControlInput2">Job Title</label>
                                    <input type="text" class="form-control" id="exampleFormControlInput2" name="comp3title"  value="{{$resume->comp3title}}">
                                </div>
                            </div>



                           <div class="col-md-6 col-lg-3">
                                <div class="my_profile_select_box form-group">

                        <label for="exampleFormControlInput3">Start Year</label><br>
                         <select name="compfromyear3" id="position" class="selectpicker">

                             @include('resumes.partials.forms.compstart3')
                                 </select>
                                </div>
                            </div>
                             <div class="col-md-6 col-lg-3">
                                <div class="my_profile_select_box form-group">

                        <label for="exampleFormControlInput3">Finished</label><br>
                         <select name="comptoyear3" id="position" class="selectpicker">

                             @include('resumes.partials.forms.compfinish3')
                                 </select>
                                </div>
                            </div>


                                                         <div class="col-lg-12">
                                <h4 class="fz20 mb20">Add your other work experience</h4>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <div class="my_profile_input form-group">
                                    <label for="formGroupExampleInput1">Company Name</label>
                                    <input type="text" class="form-control" id="formGroupExampleInput1" name="comp4"  value="{{$resume->comp4}}">
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <div class="my_profile_input form-group">
                                    <label for="formGroupExampleInput2">City/Town</label>
                                    <input type="text" class="form-control" id="formGroupExampleInput2" name="comp4loc"  value="{{$resume->comp4loc}}">
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <div class="my_profile_input form-group">
                                    <label for="exampleFormControlInput2">Job Title</label>
                                    <input type="text" class="form-control" id="exampleFormControlInput2" name="comp4title"  value="{{$resume->comp4title}}">
                                </div>
                            </div>



                           <div class="col-md-6 col-lg-3">
                                <div class="my_profile_select_box form-group">

                        <label for="exampleFormControlInput3">Start Year</label><br>
                         <select name="compfromyear4" id="position" class="selectpicker">

                             @include('resumes.partials.forms.compstart4')
                                 </select>
                                </div>
                            </div>
                             <div class="col-md-6 col-lg-3">
                                <div class="my_profile_select_box form-group">

                        <label for="exampleFormControlInput3">Finished</label><br>
                         <select name="comptoyear4" id="position" class="selectpicker">

                             @include('resumes.partials.forms.compfinish4')
                                 </select>
                                </div>
                            </div>



                             <input type="hidden" name="area_id" value="50">
                              <input type="hidden" id="custId" name="category_id" value="{{$resume->category_id}}">
                             <input type="hidden" id="custId" name="edutitle1" value="{{$resume->edutitle1 }}">
                             <input type="hidden" id="custId" name="edutitle2" value="{{$resume->edutitle2 }}">
                             <input type="hidden" id="custId" name="edutitle3" value="{{$resume->edutitle3 }}">
                             <input type="hidden" id="custId" name="edutitle4" value="{{$resume->edutitle4 }}">
                             <input type="hidden" id="custId" name="edulevel1" value="{{$resume->edulevel1}}">
                             <input type="hidden" id="custId" name="edulevel2" value="{{$resume->edulevel2}}">
                             <input type="hidden" id="custId" name="edulevel3" value="{{$resume->edulevel3}}">
                             <input type="hidden" id="custId" name="edulevel4" value="{{$resume->edulevel4}}">
                              <input type="hidden" id="custId" name="edufromyear1" value="{{$resume->edufromyear1}}">
                               <input type="hidden" id="custId" name="edufromyear2" value="{{$resume->edufromyear2}}">
                                <input type="hidden" id="custId" name="edufromyear3" value="{{$resume->edufromyear3}}">
                                 <input type="hidden" id="custId" name="edufromyear4" value="{{$resume->edufromyear4}}">
                             <input type="hidden" id="custId" name="edutoyear1" value="{{$resume->edutoyear1 }}">
                             <input type="hidden" id="custId" name="edutoyear2" value="{{$resume->edutoyear2 }}">
                             <input type="hidden" id="custId" name="edutoyear3" value="{{$resume->edutoyear3 }}">
                             <input type="hidden" id="custId" name="edutoyear4" value="{{$resume->edutoyear4 }}">
                             <input type="hidden" id="custId" name="institution1" value="{{$resume->institution1}}">
                             <input type="hidden" id="custId" name="institution2" value="{{$resume->institution2}}">
                             <input type="hidden" id="custId" name="institution3" value="{{$resume->institution3}}">
                             <input type="hidden" id="custId" name="institution4" value="{{$resume->institution4}}">



<input type="hidden" id="custId" name="nxtjobtitle" value="{{$resume->nxtjobtitle}}">
                              <input type="hidden" id="custId" name="nxtjobrelocate" value="{{$resume->nxtjobrelocate}}">
                               <input type="hidden" id="custId" name="nxtjobtype" value="{{$resume->nxtjobtype}}">


                            <div class="col-lg-4">
                                <div class="my_profile_input">
                                    <button class="btn btn-lg btn-thm" type="submit">Save Employment</button>
                                </div>
                            </div>

                             {{ csrf_field() }}
                             @method('PATCH')
                        </form><!-- form -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



@endsection


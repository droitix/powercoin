@extends('layouts.app')
@section('title')

@endsection
@section('content')
<!-- Sidebar -->
            <div class="sidebar" id="sidebar">
                <div class="sidebar-inner slimscroll">
                    <div id="sidebar-menu" class="sidebar-menu">
                        <ul>

                            <li class="active">
                                <a href="{{url('/')}}"><i class="fe fe-home"></i> <span>Dashboard</span></a>
                            </li>
                            @role('admin')
                            <li>
                                <a href="{{ route('listings.create', [$area]) }}"><i class="fe fe-lock"></i> <span>Create Bid</span></a>
                            </li>
                              <li>
                                <a href="{{ url('admin/impersonate')}}"><i class="fe fe-lock"></i> <span>Impersonate</span></a>
                            </li>
                            @endrole
                            @if (session()->has('impersonate'))
                              <li>
                                <a href="{{ route('listings.create', [$area]) }}"><i class="fe fe-lock"></i> <span>Create Bid</span></a>
                            </li>
                            @endif
                            @if (session()->has('impersonate'))
                        <li>
                            <a href="#" onclick="event.preventDefault(); document.getElementById('impersonating').submit();"><i class="fe fe-lock"></i> Stop Impersonating</a>
                        </li>
                        <form action="{{ route('admin.impersonate') }}" class="hidden" method="POST" id="impersonating">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                        </form>

              @endif

                            <li>
                                <a href="{{url('bidding')}}"><i class="fe fe-bell"></i> <span>Bidding Rooms</span></a>
                            </li>
                             <li>
                                <a href="{{ route('comments.published.index') }}"><i class="fe fe-document"></i> <span>My Bids</span></a>
                            </li>
                             <li>
                                <a href="{{ route('listings.unpublished.index', [$area]) }}"><i class="fe fe-tiled"></i> <span>Banked Coins</span></a>
                            </li>
                             <li>
                                <a href="{{ route('listings.published.index', [$area]) }}"><i class="fe fe-money"></i> <span>Selling Coins</span></a>
                            </li>
                             <li>
                                <a href="{{ route('listings.history.index', [$area]) }}"><i class="fe fe-file"></i> <span>All Transactions</span></a>
                            </li>


                            <li>
                                <a href="{{ route('profile') }}"><i class="fe fe-user-plus"></i> <span>Profile</span></a>
                            </li>
                                <li><a  href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                            <i class="fe fe-logout"></i>{{ __('Logout') }}
                                        </a></li>

                                         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- /Sidebar -->



            <!-- Page Wrapper -->
            <div class="page-wrapper">

                <div class="content container-fluid">

                    <!-- Page Header -->
                    <div class="page-header">
                        <div class="row">
                            <div class="col">
                                <h3 class="page-title">Sell My Coins </h3>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a></li>
                                    <li class="breadcrumb-item active">Sell Coins</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /Page Header -->





                   <div class="col-md-6">
                            <div class="card">
                                <div class="card-header">
                                     @if ($listing->type())
                                    <h4 class="card-title">You are selling Your Coins for ${{$listing->amount}}</h4>

                                    @else
                                   <h4 class="card-title">You are selling Your Coins for R{{$listing->amount}}</h4>
                                    @endif
                                </div>
                                <div class="card-body">
                               <form action="{{ route('listings.update', [$area, $listing]) }}" method="post">

                                         <input type="hidden" name="category_id" value="{{ $listing->category_id }}">
                                          <input type="hidden" name="amount" class="form-control" value="{{ $listing->amount }}">
                                         <input type="hidden" class="form-control" name="area_id" id="area" value="5">

                                            <button type="submit" class="btn btn-primary">Sell now</button>

                                         {{ csrf_field() }}
                        {{ method_field('PATCH') }}
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- /Page Wrapper -->


@endsection

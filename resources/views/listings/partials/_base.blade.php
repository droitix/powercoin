<div class="col-sm-12 col-lg-12">
    <a href="{{ route('listings.show', [$area, $listing,$listing->slug]) }}">
                    <div class="fj_post">
                        <div class="details">
                            <h5 class="job_chedule text-thm mt0">{{$listing->position}}</h5>

                            <h4>{{$listing->jobtitle}}</h4>
                            <p>Posted {{$listing->created_at->diffForHumans()}} by <a class="text-thm" href="#">{{$listing->companyname}}</a></p>
                            <ul class="featurej_post">
                                <li class="list-inline-item"><span class="flaticon-location-pin"></span> <a href="{{ route('listings.show', [$area, $listing,$listing->slug]) }}">{{ $listing->area->parent->name }}, {{ $listing->area->name }}</a></li>
                                <li class="list-inline-item"><span class="flaticon-price pl20"></span> <a href="{{ route('listings.show', [$area, $listing,$listing->slug]) }}">{{$listing->salary}}</a></li>
                            </ul>
                        </div>
                        <a class="btn btn-md btn-transparent float-right fn-smd" href="{{ route('listings.show', [$area, $listing,$listing->slug]) }}">View and Apply</a>
                    </div>
                    </a>
                </div>

@extends('layouts.regapp')
@section('title')
 {{$listing->jobtitle}} in {{$listing->area->name}} with  {{$listing->companyname}}  | Openjobs360
@endsection
@section('description')
{{$listing->jobtitle}},{{$listing->area->name}}-Openjobs360 {{$listing->jobdescrip}}
@endsection
@section('keywords')
{{$listing->jobtitle}},{{$listing->area->name}},{{$listing->position}}, vacancies,  careers,  employment,  job listings,  job search,  search engine, work in ,jobs in south africa.
@endsection

@section('content')

    <!-- Candidate Personal Info Details-->
    <section class="bgc-fa pb30 mt70 mbt45">
        <div class="container">
            <div class="row">

                <div class="col-lg-8 col-xl-8">
                    <div class="candidate_personal_info style2">
                        <div class="details">
                            <span class="text-thm2 fwb">{{$listing->position}}</span>
                            <h3>{{$listing->jobtitle}}</h3>
                            <p>Posted {{$listing->created_at->diffForHumans()}} By<a href="{{route('profile.index',['user_id'=>$listing->user_id])}}" class="text-thm2"> {{$listing->companyname}}</a></p>
                            <ul class="address_list">
                                <li class="list-inline-item"><a href="#"><span class="flaticon-location-pin"></span>{{ $listing->area->parent->name }}, {{ $listing->area->name }}</a></li>
                                <li class="list-inline-item"><a href="#"><span class="flaticon-price"></span> {{$listing->salary}}</a></li>
                            </ul>
                        </div>
                        <div class="row job_meta_list mt30">


                            <div class="col-sm-4 col-lg-4"><a href="{{ route('listings.apply', [$area, $listing]) }}"><button type="submit" class="btn btn-block btn-thm">Apply Now <span class="flaticon-right-arrow pl10"></span></button></a>
                                </div>






                           @if (Auth::check())
                            @if (!$listing->favouritedBy(Auth::user()))
                            <div class="col-sm-4 col-lg-4"><button class="btn btn-block btn-gray"><span class="flaticon-favorites fz24 pr10"></span><a href="#" onclick="event.preventDefault(); document.getElementById('listings-favourite-form').submit();">Save this job</a></button></div>
                            <form action="{{ route('listings.favourites.store', [$area, $listing]) }}" method="post" id="listings-favourite-form" class="hidden">
                                        {{ csrf_field() }}
                                    </form>
                              @else
                              <div class="col-sm-4 col-lg-4"><button class="btn btn-block btn-error"><span class="flaticon-favorites fz24 pr10"></span><a href="#" onclick="event.preventDefault(); document.getElementById('listings-favourites-destroy-{{ $listing->id }}').submit();">Unsave Job</a></button></div>
                               <form action="{{ route('listings.favourites.destroy', [$area, $listing]) }}" method="post" id="listings-favourites-destroy-{{ $listing->id }}">
                                   {{ csrf_field() }}
                              {{ method_field('DELETE') }}
                                </form>
                              @endif




                            @else
                             <div class="col-sm-4 col-lg-4"><button class="btn btn-block btn-gray"><span class="flaticon-favorites fz24 pr10"></span><a href="{{url('login')}}">Save Job</a></button></div>

                            @endif
                        </div>
                        <div class="row personer_information_company">
                            <div class="col-sm-6 col-lg-6">
                                <div class="icon text-thm"><span class="flaticon-money"></span></div>
                                <div class="details">
                                    <p>Offerd Salary</p>
                                    <p>{{$listing->salary}}</p>
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-6">
                                <div class="icon text-thm"><span class="flaticon-controls"></span></div>
                                <div class="details">
                                    <p>Career Level</p>
                                    <p>Executive</p>
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-6">
                                <div class="icon text-thm"><span class="flaticon-line-chart"></span></div>
                                <div class="details">
                                    <p>Job Sector</p>
                                    <p>{{$listing->category->name}}</p>
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-6">
                                <div class="icon text-thm"><span class="flaticon-mortarboard"></span></div>
                                <div class="details">
                                    <p>Job Refference</p>
                                    <p>OJ2020{{$listing->id}}</p>
                                </div>
                            </div>



                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-xl-4">
                    <div class="candidate_personal_overview style3">

                        <ul class="company_job_list mt30 mb30">
                            <li class="list-inline-item"><a class="mt25" href="#">OpenJobs360 CV writing service</a></li>

                        </ul>
                        <p class="mb0">Get a well tailored cv that suits your industry of choice,we have a team of ex-recruiting agents who know what you need to get a good job</p>


                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-8">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="candidate_about_info style2 mt10">
                                <h4 class="fz20 mb30">Description</h4>
                                <p class="mb30">{!! nl2br(e($listing->jobdescrip)) !!}</p>

                                <p class="fwb">Background, Skills & Experience</p>
                                <p>{!! nl2br(e($listing->requirements)) !!}</p>








                                </div>


                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="my_resume_eduarea">
                                <h4 class="title mb20">People Are Also Viewing</h4>
                            </div>
                        </div>

                            @if ($listings->count())
        @foreach ($listings as $listing)
            @include ('listings.partials.base', compact('listing'))
    @endforeach



    @else
        <p>No Jobs listed yet.</p>
    @endif
            </div>

        </div>
          {{ $listings->links() }}
                        </div>



            </div>
        </div>
    </section>


@endsection

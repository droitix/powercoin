  <!-- Our Footer -->
    <section class="footer_one">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-4 col-md-3 col-lg-2">
                    <div class="quick_link_widget">
                        <h4>Quick Links</h4>
                        <ul class="list-unstyled">
                             <li><a href="{{ route('listings.allsa', [$area]) }}">All Jobs</a></li>
                             <li><a href="https://medium.com/@admin_19121">Blog</a></li>
                            <li><a href="https://medium.com/@admin_19121">Career Advice</a></li>
                            <li><a href="{{url('cv-writing')}}">CV Writing Service</a></li>



                        </ul>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-md-3 col-lg-3">
                    <div class="candidate_widget">
                        <h4>For Candidates</h4>
                        <ul class="list-unstyled">
                              <li><a href="{{url('login')}}">Login</a></li>
                            <li><a href="{{url('register')}}">Create an Account</a></li>
                             <li><a href="https://medium.com/@admin_19121">Blog</a></li>
                            <li><a href="#">Candidates Grid</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-4 col-md-4 col-md-3 col-lg-3">
                    <div class="employe_widget">
                        <h4>For Companies</h4>
                        <ul class="list-unstyled">
                             <li><a href="{{url('login')}}">Login</a></li>
                            <li><a href="{{url('companyreg')}}">Create an Account</a></li>
                            <li><a href="#">Employers Grid</a></li>
                            <li><a href="#">Browse Candidates</a></li>

                        </ul>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-4 pb25 pt25 pl30">
                    <div class="footer_social_widget mt15">
                        <p class="float-left mt10">Follow Us</p>
                        <ul>
                            <li class="list-inline-item"><a href="https://www.facebook.com/Openjobs360-102398294926787/?modal=admin_todo_tour"><i class="fa fa-facebook"></i></a></li>
                            <li class="list-inline-item"><a href="https://twitter.com/openjobs360"><i class="fa fa-twitter"></i></a></li>
                            <li class="list-inline-item"><a href="https://www.instagram.com/openjobs360/"><i class="fa fa-instagram"></i></a></li>
                            <li class="list-inline-item"><a href="#"><i class="fa fa-pinterest"></i></a></li>
                            <li class="list-inline-item"><a href="#"><i class="fa fa-dribbble"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Our Footer Bottom Area -->
    <section class="footer_bottom_area p0">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 pb10 pt10">
                    <div class="copyright-widget tac-smd mt20">
                        <p>© 2020 Openjobs360. All Rights Reserved.</p>
                    </div>
                </div>
                <div class="col-lg-8 pb10 pt10">
                    <div class="footer_menu text-right mt10">
                        <ul>


                            <li class="list-inline-item"><a href="{{url('terms')}}">Terms & Privacy Policy</a></li>

                                <select class="selectpicker show-tick">
                                    <option>English</option>

                                </select>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

<div class="main-header">
            <div class="logo">
               <a href="{{url('/')}}"><img src="/logo.png" alt=""></a>
            </div>
            <div class="menu-toggle">
                <div></div>
                <div></div>
                <div></div>
            </div>
            <div class="d-flex align-items-center">


            </div>
            <div style="margin: auto"></div>
            <div class="header-part-right">


                <!-- User avatar dropdown -->
                <div class="dropdown">
                    <div class="user col align-self-end">
                        <img src="../../dist-assets/images/faces/1.png" id="userDropdown" alt="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                            <div class="dropdown-header">
                                <i class="i-Lock-User mr-1"></i> {{Auth::user()->name}} {{Auth::user()->surname}}
                            </div>
                            @role('admin')
                                <a href="{{ url('admin/users')}}" class="dropdown-item">Admin Users</a>
                            <a href="{{ url('admin/listings')}}" class="dropdown-item">Admin Listings</a>
                             <a href="{{ url('admin/impersonate')}}" class="dropdown-item">Impersonate</a>
                            @endrole
                            <a href="{{ route('profile') }}" class="dropdown-item">Edit Profile</a>
                            <a href="{{url('bidding')}}" class="dropdown-item">Auction</a>
                             <a href="{{ route('comments.published.index') }}" class="dropdown-item">Bids</a>
                            <a href="{{ route('listings.unpublished.index', [$area]) }}" class="dropdown-item">Coin Storage</a>
                            <a href="{{ route('listings.published.index', [$area]) }}" class="dropdown-item">Sell Coins</a>

                           <a  href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                            <i class="fe fe-logout"></i>{{ __('Logout') }}
                                        </a>

                                         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>

@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')


@include('layouts.partials.sidebar')

 <div class="main-content-wrap sidenav-open d-flex flex-column">
            <!-- ============ Body content start ============= -->
            <div class="main-content">
                <div class="breadcrumb">
                    <h1>Pay Details Below</h1>
                    <ul>
                    </ul>
                </div>
                <div class="separator-breadcrumb border-top"></div>

                 <div class="row">
                    <div class="col-md-6">
                         <div class="card mb-5">
                            <div class="card-body">
                               <h4>{{$comment->listing->user->name}} {{$comment->listing->user->surname}}</h4>
                               <h4>{{$comment->listing->user->bank}} | {{$comment->listing->user->account}}</h4>
                                <h4> {{$comment->listing->user->phone}}</h4>
                            </div>
                        </div>

                <div class="border-top mb-5"></div>

                </div><!-- end of main-content -->
            </div><!-- Footer Start -->


@endsection

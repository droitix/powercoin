import autocomplete from 'autocomplete.js'
import algolia from 'algoliasearch'

var index = algolia('FUJDU5YDLX', '155f6c32db0f207ce28a970fd855958f')

export const listingsautocomplete = (selector, { categoryId, areaIds }) => {
    var listings = index.initIndex('listings')

    var areaFilters = 'area.id = ' + areaIds.join(' OR area.id = ')
    var filters = areaFilters

    if (typeof categoryId !== 'undefined') {
        filters = filters + ' AND category.id != ' + categoryId
    }

    var sources = [{
        source: autocomplete.sources.hits(listings, { hitsPerPage: 5, filters: filters + ' AND live = 1' }),
        templates: {
            header: () => {
                if (typeof categoryId !== 'undefined') {
                    return '<div class="aa-suggestions-category">Other categories</div>';
                }

                return '<div class="aa-suggestions-category">All categories</div>';
            },
            suggestion (suggestion) {
                return '<span><a href="/' + suggestion.area.slug + '/' + suggestion.id + '">' + suggestion.companyname + '</a> in ' + suggestion.category.name + '</span> <span>' + '<i class="fas fa-map-marker-alt" style="color:#886F68;"></i>' + ' ' + suggestion.area.name + ' &comma; '+ suggestion.parent_area_name +'</span>'
            }
        },
        displayKey: 'companyname',
        empty: '<div class="aa-empty">No listings found</a>'
    }];

    if (typeof categoryId !== 'undefined') {
        sources.unshift({
            source: autocomplete.sources.hits(listings, { hitsPerPage: 10, filters: '(' + areaFilters + ') AND category.id = ' + categoryId + ' AND live = 1' }),
            templates: {
                header: '<div class="aa-suggestions-category">This category</div>',
                suggestion (suggestion) {
                   return '<span><a href="/' + suggestion.area.slug + '/' + suggestion.id + '">' + suggestion.companyname + '</a> in ' + suggestion.category.name + '</span> <span>' + '<i class="fas fa-map-marker-alt" style="color:#886F68;"></i>' + ' ' + suggestion.area.name + ' &comma; '+ suggestion.parent_area_name +'</span>'
                }
            },
            displayKey: 'companyname',
            empty: '<div class="aa-empty">No listings found</a>'
        })
    }

    return autocomplete(selector, {}, sources)
}
